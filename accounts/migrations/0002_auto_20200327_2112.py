# Generated by Django 2.2.11 on 2020-03-27 21:12

from django.conf import settings
from django.db import migrations, models
import django.db.models.fields.related


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userprofile',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.fields.related.OneToOneField, related_name='profile', to=settings.AUTH_USER_MODEL),
        ),
    ]
